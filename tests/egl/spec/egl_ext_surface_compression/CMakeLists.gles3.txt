link_libraries(
	piglitutil_${piglit_target_api}
)

piglit_add_executable(egl_ext_surface_compression-create ../../egl-util.c
	../../egl-wayland.c create_surface.c)

piglit_add_executable(egl_ext_surface_compression-image ../../egl-util.c
	image_storage.c)

# vim: ft=cmake:
